![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Ejemplo de Proyecto Colaborativo.


# template-proyecto — Ejemplo de Proyecto Colaborativo con Gitlab Pages

Descripción del proyecto.
_A completar._

## Objetivos

Detallar los objetivos del proyecto. Se pueden categorizar por jerarquía o escala: principales/secundarios, macro/micro, parciales/globales, etc.
- Objetivo 1...
- Objetivo 2...

## Antecedentes
Describir antecedentes del proyecto: contexto, motivación, estado del arte, bibliografía, resultados previos, etc.

_A completar._

## Colaboradores
Describir quiénes son los Colaboradores del Proyecto. Detallar áreas de competencia, lugares de trabajo y dedicaciones horarias si corresponde.

_Ejemplo:_
- **Axel Lacapmesure:** desarrollo experimental, numérico (20 h/semana)
- ...

## Hoja de ruta

Especificar tareas en orden secuencial. Detallar subtareas.

- [1.] Tarea 1. \
  _Completar descripción._
  - [1.1.] Subtarea 1. \
    _Completar descripción._
  - [1.2.] Sub Subtarea 2. \
    _Completar descripción._
- [2.] Tarea 2. \
  _Completar descripción._

## Requisitos

Listar requisitos de instrumental, equipos, PCs, software, etc. Debe permitir generar una lista de compras.

### Lista de compras

_Ejemplo:_
- [ ] ~~Item 1 (Paraná, Mica)~~,
- [ ] Item 2 (Mercadolibre, Axel),
- ...

## Estructura del repositorio

Explicar estructura de carpetas.

_Ejemplo:_
- `public`: directorio donde alojar los HTML de Gitlab Pages.
- `Bibliografía`
  - `Hojas de datos y manuales`: información del instrumenta, sofwtare, etc., de origen propio o provisto por los fabricantes.
  - `Papers, libros y apuntes`: bibliografía de consulta.
  - ...
- `Documentación`
  - `Manuales y presentaciones`
  - `Guías de uso`
  - `Ejemplos`
- `Mediciones`
  - `(23.04.13) Mediciones iniciales`
  - `(23.04.13) Mediciones posteriores`
  - ...
- `Software`: software (código fuente, librerías, ejecutables, etc.) utilizado en el proyecto
  - `Control`
  - `Adquisición`
  - `Análisis`
  - ...
- `Diseños experimentales`

## Resultados parciales

Listar resultados parciales: nombrar resultados por hito, donde un hito debería tener (idealmente) un entregable (informe, presentación, pasada en limpio, etc.)

_Ejemplo:_
- `23.abr.13` Resultados iniciales. ([Ver informe en Drive](#link_a_drive))
- `23.abr.25` Resultados posteriores. ([Ver informe en Overleaf](#link_a_drive_2))
- ...

***

# Documentación de Gitlab: integración con Gitlab Pages

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
